#include "pugixml.hpp"

#include <iostream>
#include <fstream>

using namespace std;

bool parseActors(pugi::xml_document &doc, string filename)
{
    ofstream myfile;
    myfile.open(filename);
    myfile << "sep=,\n";
    myfile << "id,xsi_type,UniqueId,NumericalID,ObjectTypeID,HasTraj,GenTraj\n";

    pugi::xml_node tools = doc.child("Experiment").child("Actors");

    size_t ped = 0;
    size_t veh = 0;
    for (pugi::xml_node actor : tools.children("Actor"))
    {
        std::cout << "Actor:";
        bool record = false;
        int hasTraj = 0, genTraj = 0;
        for (pugi::xml_attribute attr : actor.attributes())
        {
            std::cout << " " << attr.name() << "=" << attr.value() << ", ";
            if (0 == strcmp("ObjectTypeID", attr.name()))
            {
                if (0 == strcmp("4", attr.value())) // human
                {
                    ped += 1;
                    record = true;
                }
                if (0 == strcmp("1", attr.value())) // car
                {
                    veh += 1;
                    record = true;
                }
                if (0 == strcmp("3", attr.value())) // truck
                {
                    veh += 1;
                    record = true;
                }
            }
        }
        if (actor.child("Trajectories").child("TrajectoryBase"))
        {
            auto traj = actor.child("Trajectories").child("TrajectoryBase");
            hasTraj = 1;
            if (0 == strcmp("true", traj.attribute("GenerateAbstractTrajectory").value()))
                genTraj = 1;
            else
                traj.attribute("GenerateAbstractTrajectory").set_value("true");
        }
        if (record)
            myfile << actor.attribute("id").value() << ","
                   << actor.attribute("xsi:type").value() << ","
                   << actor.attribute("UniqueId").value() << ","
                   << actor.attribute("NumericalID").value() << ","
                   << actor.attribute("ObjectTypeID").value() << ","
                   << hasTraj << "," << genTraj << "\n";
        std::cout << "\n";
        cout << "#Ped = " << ped << ", #Veh = " << veh << "\n"
             << endl;
    }
    myfile.close();
    return true;
}

bool parseTrafficSigns(pugi::xml_document &doc, string filename)
{
    ofstream myfile;
    myfile.open(filename);
    myfile << "sep=,\n";
    myfile << "id,xsi_type,UniqueId,NumericalID,ObjectTypeID\n";

    pugi::xml_node tools = doc.child("Experiment").child("InfraStructure").child("TrafficSigns");
    if (!tools)
        cerr << "The experiment doesn't have any traffic signs.\n";

    size_t ts = 0;
    for (pugi::xml_node actor : tools.children("TrafficSign"))
    {
        std::cout << "TrafficSign:";
        bool record = false;
        for (pugi::xml_attribute attr : actor.attributes())
        {
            std::cout << " " << attr.name() << "=" << attr.value() << ", ";
            if (0 == strcmp("ObjectTypeID", attr.name()))
            {
                if (0 == strcmp("14", attr.value())) // Traafic Sign
                {
                    ts += 1;
                    record = true;
                }
            }
        }
        if (record)
            myfile << actor.attribute("id").value() << ","
                   << actor.attribute("xsi:type").value() << ","
                   << actor.attribute("UniqueId").value() << ","
                   << actor.attribute("NumericalID").value() << ","
                   << actor.attribute("ObjectTypeID").value() << "\n";
        std::cout << "\n";
        cout << "#TrafficSign = " << ts << "\n"
             << endl;
    }
    myfile.close();
    return true;
}

bool modifyDoc(pugi::xml_document &doc)
{
    pugi::xml_node tools = doc.child("Experiment").child("Actors");

    for (pugi::xml_node actor : tools.children("Actor"))
    {
        for (pugi::xml_attribute attr : actor.attributes())
        {
            if (0 == strcmp("ObjectTypeID", attr.name()))
            {
                if (0 == strcmp("1", attr.value())) // car
                {
                    // test modifying
                    if (0 == strcmp("Toyota_Yaris_1", actor.attribute("id").value()))
                    {
                        actor.attribute("id").set_value("Toy_renamed");
                    }
                }
            }
        }
    }
    // save the modified doc
    cout << "Saving result: " << doc.save_file("save_file_output.pex") << endl;
    return true;
}

int main(int argc, char *argv[])
{
    cout << "Parse pex file. Usage example:\nexe a.pex [output.csv]\n"
         << endl;
    string filename;
    if (argc < 2)
        cerr << "Please specify the pex file.\n";
    if (argc < 3)
        filename = "actors.csv";
    else
        filename = argv[2];

    // tag::code[]
    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(argv[1]);

    std::cout << "Load result: " << result.description() << "\n";

    parseActors(doc, filename);
    // add a custom declaration node
    pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "utf-8";

    cout << "Saving result: " << doc.save_file(argv[1], "  ") << endl;

    parseTrafficSigns(doc, "trafficSigns.csv");
    modifyDoc(doc);
    // end::code[]
}

// vim:et
