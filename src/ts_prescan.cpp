#include "pugixml.hpp"

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Generate a list of all traffic signs in PreScan. Usage "
            "example:\nexe [prescan_version(8.5.0)] [output(ts_prescan.csv)]\n"
         << endl;
    string ver = "8.5.0";
    string filename = "ts_prescan.csv";
    if (argc > 1)
        ver = argv[1];
    if (argc > 2)
        filename = argv[2];

    string ts_path =
        "C:\\Program Files\\PreScan\\PreScan_" + ver + "\\Images\\RoadSigns";
    vector<string> categories = {"HighwaySignsD", "RoadSignsD", "RoadSignsJapan",
                                 "RoadSignsNL", "RoadSignsUS"};
    vector<string> xmlfiles = {"", "PreDefinedPlatesD", "PreDefinedPlatesJapan",
                               "PredefinedPlatesNL", "PredefinedPlatesUS"};
    string delimiter = "|";
    ofstream myfile;
    myfile.open(filename);
    myfile << "sep=" + delimiter + "\n";
    myfile << "name" + delimiter + "xsi_type" + delimiter + "PlateTexture" + delimiter + "Full Path\n";

    size_t num_ts = 0;
    for (size_t i = 1; i < categories.size(); ++i)
    {
        string path = ts_path + "\\" + categories[i] + "\\" + xmlfiles[i] + ".xml";
        cout << path << endl;

        pugi::xml_document doc;

        pugi::xml_parse_result result = doc.load_file(path.c_str());

        std::cout << "Load result: " << result.description() << "\n";

        pugi::xml_node tools = doc.child("ArrayOfPreDefinedPlate");

        for (pugi::xml_node actor : tools.children("PreDefinedPlate"))
        {
            ++num_ts;
            string full_path = ts_path + "\\" + categories[i] + "\\" + actor.attribute("PlateTexture").value();
            myfile << actor.attribute("Name").value() << delimiter
                   << actor.attribute("xsi:type").value() << delimiter
                   << actor.attribute("PlateTexture").value() << delimiter
                   << full_path << "\n";
        }
    }
    cout << num_ts << " traffic signs in total.\n";
    myfile.close();

    return 0;
}