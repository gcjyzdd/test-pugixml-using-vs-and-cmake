#include "pugixml.hpp"

#include <iostream>

int main(int argc, char* argv[])
{
// tag::code[]
    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(argv[1]);

    std::cout << "Load result: " << result.description() << ", mesh name: " << doc.child("mesh").attribute("name").value() << std::endl;
// end::code[]
}

// vim:et
