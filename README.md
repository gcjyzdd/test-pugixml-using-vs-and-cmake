# Test pugixml using VS and CMake

This is a example (template) of compiling code using Visual Studio and CMake. It covers setting include dir and link libs.

To compile:

```
mkdir build
cd build
cmake ..
cd ..
cmake --build build
```

Or

```
mkdir build64 && cd build64
cmake -G "Visual Studio 14 2015 Win64" ..
cd ..
cmake --build build64
```